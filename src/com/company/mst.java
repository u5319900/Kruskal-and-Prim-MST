package com.company;
public class mst {

    static Edge[] kruskal(double[][] g){
        Edge[] result = new Edge[g.length-1];
        DisjointSets s = new DisjointSets(g.length);
        Edge[] edges = CompleteGraph.createEdgeArray(g);
        CompleteGraph.sortEdge(edges);
        int i = 0;
        for(Edge e : edges){
            if(s.find_set(e.u)!=s.find_set(e.v)){
                result[i] = e;
                s.union(e.u,e.v);
                i++;
            }
        }
        return result;
    }

    static Edge[] prim(double[][] g){
        if(g.length != g[0].length) return null;
        Vertice[] vertices = new Vertice[g.length];
        for(int i=0;i<g.length;i++){
            vertices[i] = new Vertice(i);
            vertices[i].key =Double.POSITIVE_INFINITY;
            vertices[i].pi = null;
        }
        vertices[0].key=0; //let v[0] be the 'r'
        MinHeap q = new MinHeap(vertices);
        while(q.heapsize>0){
            Vertice u = q.extractMin();
            vertices[u.index].key = 0;
            for(int v=0;v<g.length;v++){
                if(vertices[v].key>0 && g[u.index][v]<vertices[v].key){ //v.key > 0 if v is in Q
                    int position = q.getPosition(v);
                    vertices[v].pi = u.index;
                    q.decreaseKey(position,g[u.index][v]);
                }
            }
        }
        Edge[] result = new Edge[vertices.length-1];
        for(int i=1;i<vertices.length;i++){
            result[i-1] = new Edge(i,vertices[i].pi,g[i][vertices[i].pi]);
        }
        return result;
    }

    static double getWeight(Edge[] edges){
        double weight = 0;
        for(Edge e : edges){
            weight+=e.weight;
        }
        return weight;
    }


    public static void main(String[] args) {

        int iteration = 20;
        int[] sizes = {10,100,500,1000};
        long[] kruskalTime = new long[sizes.length];
        long[] primTime = new long[sizes.length];
        double[] lnKruskal = new double[sizes.length];
        double[] lnPrim = new double[sizes.length];

        for(int i=0;i<sizes.length;i++){
            double sumWeightp = 0;
            double sumWeightk = 0;
            long sumTimeKruskal = 0;
            long sumTimePrim = 0;
            int size = sizes[i];
            double[][] graph;
            long startTimeKruskal;
            long startTimePrim;
            long endtimeKruskal;
            long endtimePrim;
            for(int j=0;j<iteration;j++){
                //generate graph
                graph = CompleteGraph.create(size);

                //Prim Algorithm
                startTimePrim = System.nanoTime();
                Edge[] prim_mst = prim(graph);
                endtimePrim = System.nanoTime();

                //Kruskal Algorithm
                startTimeKruskal = System.nanoTime();
                Edge[] kruskal_mst = kruskal(graph);
                endtimeKruskal = System.nanoTime();

                sumWeightp += getWeight(prim_mst);
                sumWeightk += getWeight(kruskal_mst);

                sumTimePrim += (endtimePrim-startTimePrim);
                sumTimeKruskal += (endtimeKruskal-startTimeKruskal);
            }
            lnKruskal[i] = sumWeightk/iteration;
            lnPrim[i] = sumWeightp/iteration;
            kruskalTime[i] = sumTimeKruskal/iteration;
            primTime[i] = sumTimePrim/iteration;
        }
        for(int i=0;i<sizes.length;i++){
            System.out.println("Kruskal: L("+sizes[i]+")="+lnKruskal[i]);
            System.out.println("Prim:    L("+sizes[i]+")="+lnPrim[i]);
        }
        for(int i=0;i<sizes.length;i++){
            System.out.println("Kruskal L("+sizes[i]+"):Time = "+kruskalTime[i]);
            System.out.println("Prim:   L("+sizes[i]+"):Time = "+primTime[i]);
        }

    }

}

class CompleteGraph{

    static double[][] create(int size){
        double[][] graph = new double[size][size];
        for(int i=0;i<size;i++){//i'th row
            for(int j=0;j<size;j++){//j'th column
                if(j>i){//upper triangle
                    graph[i][j] = Math.random();
                }else if(j==i){//diagonal
                    graph[i][j] = 0;
                }else{//lower triangle
                    graph[i][j] = graph[j][i]; //to be symmetric
                }
            }
        }
        return graph;
    }

    static Edge[] createEdgeArray(double[][] weights){
        if(weights.length != weights[0].length){
            return null;
        }else{
            int size = weights.length;
            int edges_size = size * (size - 1) / 2;
            Edge[] edges_array = new Edge[edges_size];
            int index = 0;
            for (int i = 0; i < size - 1; i++) {
                for (int j = i + 1; j < size; j++) {
                    edges_array[index] = new Edge(i, j, weights[i][j]);
                    index++;
                }
            }
            return edges_array;
        }
    }

    static void sortEdge(Edge[] e){
        quickSort(e,0,e.length-1);
    }

    static void quickSort(Edge[] e, int p, int r){// a quick sort algorithm for edges
        if(p<r){
            int q = partition(e,p,r);
            quickSort(e,p,q-1);
            quickSort(e,q+1,r);
        }
    }

    static int partition(Edge[] e, int p, int r){
        double x = e[r].weight;
        int i = p-1;
        Edge tmp;
        for(int j=p;j<r;j++){
            if(e[j].weight<=x){
                i++;
                tmp = e[i];
                e[i] = e[j];
                e[j] = tmp;
            }
        }
        tmp = e[i+1];
        e[i+1] = e[r];
        e[r] = tmp;
        return i+1;
    }

}

class Edge{
    int u,v;
    double weight;
    Edge(int u,int v,double w){
        this.u=u;
        this.v=v;
        this.weight=w;
    }
}

class Vertice{
    int index;
    double key = Double.POSITIVE_INFINITY;
    Integer pi = null;
    Vertice(int index) {
        this.index = index;
    }

}

class DisjointSets{
    int[] rank;
    int[] parent;

    DisjointSets(int size){
        rank = new int[size];
        parent = new int[size];

        //Make Set
        for(int r : rank) r = 0;
        for(int i = 0; i<size; i++) parent[i] = i;
    }

    int find_set(int x){
        if(x != parent[x]) parent[x] = find_set(parent[x]);
        return parent[x];
    }

    void link(int x, int y){
        if(rank[x] > rank[y]){
            parent[y] = x;
        }else{
            parent[x] = y;
            if(rank[x] == rank[y]) rank[y]++;
        }
    }

    void union(int x, int y){
        link(find_set(x),find_set(y));
    }


}

class MinHeap{
    int heapsize = 0;
    int arraySize;
    Vertice[] a;
    int[] positions;

    MinHeap(Vertice[] vertices){//Build-Min-Heap
        heapsize = vertices.length;
        arraySize = vertices.length+1;
        a = new Vertice[heapsize+1];
        positions = new int[vertices.length];
        for(int i=0; i<heapsize;i++){
            a[i+1]=vertices[i];
            positions[i]=i+1;
        }
        for(int i = vertices.length/2;i>0;i--){
            minHeapify(i);
        }
    }

    Vertice extractMin(){
        Vertice min = a[1];
        a[1]=a[heapsize];
        heapsize--;
        minHeapify(1);
        return min;
    }

    void minHeapify(int i){
        int l=left(i);
        int r=right(i);
        int smallest=i;
        if(l<=heapsize){
            if(a[l].key<a[i].key){
                smallest=l;
            }
        }
        if(r<=heapsize){
            if(a[r].key<a[smallest].key){
                smallest=r;
            }
        }
        if(smallest!=i){
            exchange(i,smallest);
            minHeapify(smallest);
        }
    }

    int getPosition(int index){
        return positions[index];
    }

    void decreaseKey(int index, double key){
        int i = index;
        if( key <= a[i].key){
            a[i].key=key;
            while( i > 1 && a[parent(i)].key > a[i].key){
                exchange(i,parent(i));
                i = parent(i);
            }
        }
    }


    static int parent(int i){
        return i/2;
    }

    static int left(int i){
        return 2*i;
    }

    static int right(int i){
        return 2*i + 1;
    }

    void exchange(int l, int r){
        Vertice tmp = a[l];
        a[l] = a[r];
        a[r] = tmp;
        positions[a[r].index]=r;
        positions[a[l].index]=l;
    }
}